class Book < ApplicationRecord
  validates :stock, :author, :category, :name, presence: true
  belongs_to :author
  belongs_to :category
  has_many :reservations, dependent: :destroy
  validates :stock, numericality: { only_integer: true, greater_tham_or_equal_to: 0}
end