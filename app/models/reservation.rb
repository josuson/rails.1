class Reservation < ApplicationRecord
  validates :librarian, :book, :client, presence: true
  belongs_to :book
  belongs_to :client
  belongs_to :librarian
  
  
  before_create :subtrai_estoque
  after_destroy :soma_estoque

  private

  def subtrai_estoque
    #Apresenta que o estoque ta vazio
    if self.book.stock < 1
      throw(:abort)
    end
    #retira 1 do estoque
    self.book.update(stock: book.stock-1)
    end
  end

  def soma_estoque
    #Vai acrescentar 1 no estoque quando for criado um novo livro
    self.book.update(stock: book.stock+1)
end