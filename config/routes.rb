Rails.application.routes.draw do
  get 'home/index'
  resources :books
  resources :categories
  resources :authors
  resources :clients
  resources :reservations
  devise_for :librarians
  root 'books#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
