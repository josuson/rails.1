class AddNameToLibrarians < ActiveRecord::Migration[5.2]
  def change
    add_column :librarians, :name, :string
  end
end
