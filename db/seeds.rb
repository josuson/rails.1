# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#   
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#   Usando a Gem faker irei gerar novos nomes, números, e categorias aleatórios.
    require 'faker'
    puts "Novos clientes"
    8.times do |i|
        Client.create!( 
            name: Faker::Name.name
             )
        Category.create!( 
            name: Faker::Book.genre
            )
        Author.create!( 
            name: Faker::Name.name
             )
    end
    puts "Novos livros"
    8.times do |i|
        Book.create!( 
              name: Faker::Book.title, 
              author: Author.all.sample(), 
              category: Category.all.sample(), 
              stock: Random.rand(1..8)
             )
    end
    puts "Novos Bibliotecarios"
    8.times do |i|
        Librarian.create!(
            name: Faker::Name.name, 
            email: Faker::Internet.email, 
            password:Faker::Internet.password(min_length: 8) 
        )
    end
    puts "Novas Reservas"
    8.times do |i|
        Reservation.create!( 
              book: Book.all.sample(), 
              librarian: Librarian.all.sample(), 
              client: Client.all.sample()
             )
    end
    
